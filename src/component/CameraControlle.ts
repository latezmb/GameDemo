import GameMingCtr from "../controller/GameMingCtr";

export default class CameraControlle extends Laya.Script {

    public PlayerCamreDiffer: Laya.Vector3;

    constructor() { super(); }
    
    onAwake() {
        this.PlayerCamreDiffer = new Laya.Vector3(4.360669999999999, 15.022839999999999, -0.6786012);
        // let _player = GameSceneCtr.Instance.Player.transform.position;
        // let c = (this.owner as Laya.Camera).transform.position;
        // let _d = new Laya.Vector3();
        // Laya.Vector3.subtract(c, _player, _d)
        // console.log(_d);
        (this.owner as Laya.Camera).fieldOfView = 100;
        (this.owner as Laya.Camera).nearPlane = 0.01;
    }

    onUpdate() {
        this.onFollowPlayer();
    }

    /**摄像机跟随主角 */
    onFollowPlayer() {
        let _player = GameMingCtr.Instance.Player.transform.position;
        let _newPos = new Laya.Vector3();
        Laya.Vector3.add(_player, this.PlayerCamreDiffer, _newPos);
        (this.owner as Laya.Camera).transform.position = _newPos;
    }

}