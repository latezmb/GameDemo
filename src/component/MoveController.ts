import GameMingCtr, { GameState } from "../controller/GameMingCtr";

export default class MoveController extends Laya.Script {

    public FirstPoint: Laya.Vector2;
    public MovePoint: Laya.Vector2;
    public Angle;
    public isClick: boolean;

    constructor() { super(); }
    
    onAwake() {
        this.FirstPoint = new Laya.Vector2();
        this.MovePoint = new Laya.Vector2();
        this.Angle = null;
        this.isClick = false;

        Laya.stage.on(Laya.Event.MOUSE_DOWN, this, this.onMouseDown);
        Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
        Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onMouseUp);
    }

    onMouseDown() {
        this.isClick = true;
        this.FirstPoint = new Laya.Vector2(Laya.stage.mouseX, Laya.stage.mouseY);
    }

    onMouseMove() {
        if(!this.isClick) return;
        this.MovePoint = new Laya.Vector2(Laya.stage.mouseX, Laya.stage.mouseY);
        let _diff = new Laya.Vector2(this.MovePoint.x - this.FirstPoint.x, this.MovePoint.y - this.FirstPoint.y);
        let _ang = new Laya.Vector2();
        Laya.Vector2.normalize(_diff, _ang);
        this.Angle = _ang;
        // console.log(this.Angle);
    }

    onMouseUp() {
        this.Angle = null;
        this.isClick = false;
    }

}