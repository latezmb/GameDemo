import MoveController from "./MoveController";
import GameMingCtr, { GameState } from "../controller/GameMingCtr";

export default class PlayerModel extends Laya.Script3D {

    public MoveCtr: MoveController;
    public Speed: number = 0.1;
    public P_Obj: Laya.Sprite3D;

    public s_y: Laya.Vector3;
    public x_y: Laya.Vector3;

    // 中间变量
    private mRotateQuaternion: Laya.Quaternion = new Laya.Quaternion();
    private mRotateQuaternion1: Laya.Quaternion = new Laya.Quaternion();
    private mRotateQuaternion2: Laya.Quaternion = new Laya.Quaternion();

    /**记录上一帧的角度 */
    public lastAngle: number = 0;
    public thisAngle: number = 0;
    public rotateAngle: number = 2;

    constructor() { super(); }

    onUpdate() {
        this.onMove();   
        this.onAngle(); 
    }

    onAwake() {
        this.MoveCtr = this.owner.getComponent(MoveController) as MoveController;
        this.P_Obj = this.owner as Laya.Sprite3D;
        this.lastAngle = 180;
    }

    /**角色移动 */
    onMove() {
        if(GameMingCtr.Instance.GameState != GameState.GameMing) return;
        if(!this.MoveCtr.Angle) return;
        if(GameMingCtr.Instance.isBoundary) return;
        // let _x = this.P_Obj.transform.position.x + this.MoveCtr.Angle.y * this.Speed;
        // let _y = this.P_Obj.transform.position.y;
        // let _z = this.P_Obj.transform.position.z -this.MoveCtr.Angle.x * this.Speed;
        // this.P_Obj.transform.position = new Laya.Vector3(_x, _y, _z);
        // this.P_Obj.transform.translate(new Laya.Vector3(this.MoveCtr.Angle.x * this.Speed, 0, this.MoveCtr.Angle.y * this.Speed));
        let _pos = GameMingCtr.Instance.GetLocalForward(this.P_Obj.transform.position, this.thisAngle + 90, 0.1);

        this.P_Obj.transform.position = _pos;
    } 

    /**角色旋转 */
    onAngle() {
        if(GameMingCtr.Instance.GameState != GameState.GameMing) return;
        if(!this.MoveCtr.Angle) return;
        this.thisAngle = Math.atan2(this.MoveCtr.Angle.x, this.MoveCtr.Angle.y) * 180 / Math.PI;

        if(Math.abs(this.thisAngle - this.lastAngle) > 90) {
            if(this.thisAngle > this.lastAngle) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
                if(this.thisAngle <= -180) {
                    this.thisAngle += 360
                }
            }else {

                this.thisAngle = this.lastAngle + this.rotateAngle;
                if(this.thisAngle > 180) {
                    this.thisAngle -= 360;
                }
            }
        }
        
        if ((this.thisAngle >= 0 && this.lastAngle >= 0) || (this.thisAngle < 0 && this.lastAngle < 0)) {
            if(this.thisAngle > this.lastAngle) {
                this.thisAngle = this.lastAngle + this.rotateAngle;
            }
            else {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
        }
        else {
            if(this.thisAngle == 0 && this.lastAngle > 0) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
            if(this.thisAngle == 0 && this.lastAngle < 0) {
                this.thisAngle = this.lastAngle + this.rotateAngle;
            }
            if(this.thisAngle == 180 && this.lastAngle < 0) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
            if(this.thisAngle == 180 && this.lastAngle > 0) {
                this.thisAngle = this.lastAngle + this.rotateAngle;
            }
            if(this.thisAngle == 180 && this.lastAngle == 0) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
            if(this.thisAngle == 0 && this.lastAngle == 180) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
            if(this.thisAngle == 90 && this.lastAngle == -90) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
            if(this.thisAngle == -90 && this.lastAngle == 90) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }

            //对角象限边界
            if((this.thisAngle <= 180 && this.thisAngle >= 90) && (this.lastAngle <= 0 && this.lastAngle >= -90)) {
                if(this.thisAngle < this.lastAngle + 180) {
                    this.thisAngle = this.lastAngle + this.rotateAngle;
                }
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
            if((this.thisAngle <= 90 && this.thisAngle >= 0) && (this.lastAngle <= -90 && this.lastAngle >= -180)) {
                if(this.thisAngle < this.lastAngle + 180) {
                    this.thisAngle = this.lastAngle + this.rotateAngle;
                }
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }

            //对角象限边界
            if((this.thisAngle <= -180 && this.thisAngle >= -90) && (this.lastAngle >= 0 && this.lastAngle >= 90)) {
                if(this.thisAngle > this.lastAngle - 180) {
                    this.thisAngle = this.lastAngle + this.rotateAngle;
                }
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }
            if((this.thisAngle <= 90 && this.thisAngle >= 0) && (this.lastAngle <= -90 && this.lastAngle >= -180)) {
                if(this.thisAngle > this.lastAngle - 180) {
                    this.thisAngle = this.lastAngle + this.rotateAngle;
                }
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }

            //上方 90~180
            if((this.thisAngle <= 180 && this.thisAngle >= 90) && (this.lastAngle >= -180 && this.lastAngle <= -90)) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
                if(this.thisAngle <= -180) {
                    this.thisAngle = 180;
                }
            }
            if((this.thisAngle <= 90 && this.thisAngle >= 0) && (this.lastAngle >= -90 && this.lastAngle <= 0)) {
                this.thisAngle = this.lastAngle + this.rotateAngle;
            }

            if((this.thisAngle >= -180 && this.thisAngle <= -90) && (this.lastAngle <= 180 && this.lastAngle >= 90)) {
                this.thisAngle = this.lastAngle + this.rotateAngle;
                if(this.thisAngle > 180) {
                    this.thisAngle = -180;
                }
            }
            if((this.thisAngle <= 0 && this.thisAngle >= -90) && (this.lastAngle >= 0 && this.lastAngle <= 90)) {
                this.thisAngle = this.lastAngle - this.rotateAngle;
            }


        }
        
        // console.log(this.thisAngle);
        
        this.mRotateQuaternion1 = this.P_Obj.transform.rotation;
        Laya.Quaternion.createFromYawPitchRoll((this.thisAngle) * (Math.PI / 180) , this.P_Obj.transform.rotationEuler.x * (Math.PI / 180), this.P_Obj.transform.rotationEuler.z * (Math.PI / 180), this.mRotateQuaternion2);
        Laya.Quaternion.lerp(this.mRotateQuaternion1, this.mRotateQuaternion2, 0.2, this.mRotateQuaternion);
        this.P_Obj.transform.rotation = this.mRotateQuaternion;


        this.lastAngle = this.thisAngle;
    }

    /**获得当前位置 */
    get pos(): Laya.Vector3 {
        return this.P_Obj.transform.position;
    }

}