import Main from "../Main";
import PlayerModel from "../component/PlayerModel";
import MoveController from "../component/MoveController";
import CameraControlle from "../component/CameraControlle";
import { ui } from "../ui/layaMaxUI";
import Dictionary from "../utils/Dictionary";
import CommonManagerAPI from "../utils/CommonManagerAPI";

export default class GameMingCtr extends Laya.Sprite {

    public static Instance: GameMingCtr;

    //游戏中用到
    public MainScene: Laya.Scene3D;
    public MainCamera: Laya.Camera;
    public Player: Laya.Sprite3D;
    public BlockList: Array<Laya.Sprite3D>;
    public lastPoint: Laya.Vector3;
    public displacementDiff: number;
    public BlockModel: Laya.Sprite3D;
    public endModel: Laya.Sprite3D;
    public GameState: GameState;
    public ListIndex = 0;
    public isBoundary: boolean = false;

    //射线
    public mRay: Laya.Ray = null;
    private mRayHitResult: Laya.HitResult = null;
    private mRayPhysics: Laya.PhysicsSimulation = null;

    /**存储方块点信息 */
    public BlockPointInfo: Dictionary;

    constructor() { 
        super();
        GameMingCtr.Instance = this;
        this.MainScene = new Laya.Scene3D;
        this.MainCamera = new Laya.Camera;
        this.BlockList = [];
        this.Init();
        this.displacementDiff = 0.8;
        this.lastPoint = this.Player.transform.position.clone();
        Laya.timer.frameLoop(1, this, this.onUpdate);
        this.BlockPointInfo = new Dictionary();

        this.mRay = new Laya.Ray(new Laya.Vector3(0, 0, 0), new Laya.Vector3(0, 0, 0));
        this.mRayHitResult = new Laya.HitResult();
        this.mRayPhysics = (this.MainScene as Laya.Scene3D).physicsSimulation;

        this.GameState = GameState.GameMing;

        //摄像机控制
        Laya.stage.on(Laya.Event.KEY_DOWN, this, (e) => {
            // 87w 、83s、65a、68d     37左、 38上、39右、40下、-109、+107
            let _euler = this.MainCamera.transform.localRotationEuler;
            let _pos = this.MainCamera.transform.position;
            let _value = 1;
            console.log(e.keyCode);
            switch(e.keyCode) {
                case 87: 
                    this.MainCamera.transform.localRotationEuler = new Laya.Vector3(_euler.x + _value, _euler.y, _euler.z);
                    break;
                case 83: 
                    this.MainCamera.transform.localRotationEuler = new Laya.Vector3(_euler.x - _value, _euler.y, _euler.z);
                    break;
                case 65:
                    this.MainCamera.transform.localRotationEuler = new Laya.Vector3(_euler.x, _euler.y + _value, _euler.z);
                    break;
                case 68:
                    this.MainCamera.transform.localRotationEuler = new Laya.Vector3(_euler.x, _euler.y - _value, _euler.z);
                    break;  
                case 37:
                    this.MainCamera.transform.position = new Laya.Vector3(_pos.x, _pos.y, _pos.z + _value);
                    break;
                case 39:
                    this.MainCamera.transform.position = new Laya.Vector3(_pos.x, _pos.y, _pos.z - _value);
                    break;
                case 38:
                    this.MainCamera.transform.position = new Laya.Vector3(_pos.x + _value, _pos.y, _pos.z);
                    break;
                case 40:
                    this.MainCamera.transform.position = new Laya.Vector3(_pos.x - _value, _pos.y, _pos.z);
                    break;
                case 109:
                    this.MainCamera.transform.position = new Laya.Vector3(_pos.x, _pos.y - _value, _pos.z);
                    break;
                case 107:
                    this.MainCamera.transform.position = new Laya.Vector3(_pos.x, _pos.y + _value, _pos.z);
                    break;
            }
        })
    }

    onUpdate() {

        switch(this.GameState) {
            case GameState.GameMing:
                this.onGameMing();
                break;
            case GameState.Lose:
                this.onGameOver();
                break;
        }
           
    }

    onGameMing() {
        this.onNewBlock();
        this.onRay(); 
    }

    onWin() {
        Laya.timer.frameLoop(10, this, this.onModeFall)
    }
    
    onModeFall() {
        let _targer = null;
        if(this.ListIndex >= this.BlockList.length){
            _targer = this.Player;
        }else {
            _targer = this.BlockList[this.ListIndex];
        }
        let _euler = _targer.transform.localRotationEuler;
        let _before = {
            z: _euler.z
        }
        let _later = {
            z: -80,
            update: new Laya.Handler(this, () => {
                _targer.transform.localRotationEuler = new Laya.Vector3(_euler.x, _euler.y, _before.z);
            })
        }
        Laya.Tween.to(
            _before,
            _later,
            1000
        )
        this.ListIndex += 1;
        if(this.ListIndex == this.BlockList.length) {
            Laya.timer.clear(this, this.onModeFall);
            this.onModeFall();
        }
    }
    
    onAwake() {
        
    }

    Init() {
        Laya.stage.addChild(this.MainScene);
        let _model = Main.ScenneModel.clone() as Laya.Sprite3D;
        this.MainCamera = _model.getChildByName("Main Camera") as Laya.Camera;
        this.MainCamera.addComponent(CameraControlle);

        this.BlockModel = _model.getChildByName("Cube") as Laya.Sprite3D;
        this.BlockModel.active = false;

        this.Player = (_model.getChildByName("Cube") as Laya.Sprite3D).clone() as Laya.Sprite3D;
        this.Player.addComponent(MoveController);
        this.Player.addComponent(PlayerModel);
        let _eulrt = this.Player.transform.localRotationEuler
        this.Player.transform.localRotationEuler = new Laya.Vector3(_eulrt.x, _eulrt.y + 180, _eulrt.z);
        this.Player.active = true;

        this.endModel = _model.getChildByName("end") as Laya.Sprite3D;

        _model.addChild(this.Player);

        this.MainScene.addChild(_model);
    }

    /**生成新的模型 */
    onNewBlock() {
        let _playerPos = this.Player.transform.position;
        let _diff = new Laya.Vector3();
        Laya.Vector3.subtract(_playerPos, this.lastPoint, _diff);

        //使用距离向量求距离
        let _distance = Math.sqrt(Math.pow(_diff.x, 2) + Math.pow(_diff.z, 2));
        if(_distance < this.displacementDiff) return;

        //生成新板块
        let _euler = this.Player.transform.localRotationEuler.clone();
        let _newBlock = this.BlockModel.clone();
        _newBlock.active = true;
        (_newBlock as Laya.Sprite3D).transform.localRotationEuler = _euler;
        (_newBlock as Laya.Sprite3D).transform.position = _playerPos;
        this.MainScene.addChild(_newBlock);
        this.BlockList.push(_newBlock as Laya.Sprite3D);
        
        this.lastPoint = _playerPos.clone();

        let _test = this.Player.getComponent(PlayerModel) as PlayerModel;
        
        _test.s_y = (_newBlock as Laya.Sprite3D).transform.localRotationEuler;
        // let _mesh = _newBlock as Laya.MeshSprite3D;
        // let _meshSize = _mesh.meshRenderer.bounds.getExtent();
        // this.onSetColliderMap(_meshSize, _playerPos);
    }

    /**生成新模型晃动 */
    onShaking(block) {

    }

    /**碰撞模型 */
    // collisionModel() {
    //     let _pos = CommonManagerAPI.ScaleVector(this.Player.transform.position, 100);
    //     let _mapInfo = this.BlockPointInfo.get(`x${Math.round(_pos.x)}z${Math.round(_pos.z)}`) || [];
    //     if(_mapInfo.length != 0) {
    //         this.onGameOver();
    //     }
    // }

    /**记录生成模型的信息 */
    // onSetColliderMap(_meshSize, _pos) {
    //     let _sizeX = Math.floor(_meshSize.x * 100);
    //     let _sizeZ = Math.floor(_meshSize.z * 100);
    //     if(_sizeX < 1) {
    //         _sizeX = 1;
    //     }
    //     if(_sizeZ < 1) {
    //         _sizeZ = 1;
    //     }
    //     let _mapBasePoint = CommonManagerAPI.ScaleVector(_pos, 100);
    //     let _pointArr = CommonManagerAPI.GetBoxPoint(Math.round(_mapBasePoint.x), Math.round(_mapBasePoint.z), _sizeX, _sizeZ);
    //     for(let element of _pointArr) {
    //         let _boxArr = this.BlockPointInfo.get(`x${element.x}z${element.y}`) || [];
    //         _boxArr.push(1);
    //         this.BlockPointInfo.set(`x${element.x}z${element.y}`, _boxArr);
    //     }

    // }

    /**游戏结束 */
    onGameOver() {
        console.log("游戏结束")
    }

    /**射线跟随玩家前方 */
    onRay() {
        let _playerPos = this.Player.transform.position;
        let _pos = new Laya.Vector3(_playerPos.x, _playerPos.y + 0.5, _playerPos.z)
        let _pMoveCtr = this.Player.getComponent(MoveController) as MoveController;
        let _p = this.Player.getComponent(PlayerModel) as PlayerModel;
        if(_pMoveCtr.Angle == null) {
            return;
        }
        let _angle = _p.thisAngle;
        let _startPos = this.GetLocalForward(_pos, _angle + 90, 0.8);
        let _endPos = this.GetLocalForward(_pos, _angle + 90, 1);
        this.mRay.origin = _startPos;
        this.mRay.direction = _endPos;
        if(this.mRayPhysics.rayCast(this.mRay, this.mRayHitResult, 0.2)) {
            // console.log(_playerPos);
            // console.log(this.mRayHitResult);
            let _target = this.mRayHitResult.collider.owner as Laya.Sprite3D;
            if(_target == this.endModel) {
                this.GameState = GameState.Win;
                Laya.timer.once(1000, this, () => {Laya.stage.on(Laya.Event.CLICK, this, this.onWin)});
                return;
            }
            for (let _block of this.BlockList) {
                // console.log(_block.getChildByName("Cube") as Laya.Sprite3D);
                if(_target == _block.getChildByName("Cube") as Laya.Sprite3D) {
                    this.GameState = GameState.Lose;
                    break;
                }
            }
            console.log(_target);
            if(_target.name = "boundary") {
                this.isBoundary = true;
            }
            else {
                this.isBoundary = false;
            }
        }
        else {
            this.isBoundary = false;
        }
        //射线可视化
        var lineSprite:Laya.PixelLineSprite3D = this.MainScene.addChild(new Laya.PixelLineSprite3D(1)) as Laya.PixelLineSprite3D;
        lineSprite.addLine(_startPos, _endPos, Laya.Color.RED, Laya.Color.RED);
    }

        /**
    * 获取本地前方坐标
    * @param startPoint 起点位置
    * @param angle 角度
    * @param distance 长度 默认值:10
    * @return 本地坐标系前方坐标
    */
    public GetLocalForward(startPoint:Laya.Vector3,angle:number,distance:number = 10):Laya.Vector3{
        let localForward:Laya.Vector3 = new Laya.Vector3();
        let radian =(angle * Math.PI)/180;
        localForward.x = startPoint.x + distance * Math.sin(radian);
        localForward.y = startPoint.y;
        localForward.z = (startPoint.z + distance * Math.cos(radian));
        return localForward;
    }
    
}

export enum GameState {
    GameMing,
    Win,
    Lose,
}