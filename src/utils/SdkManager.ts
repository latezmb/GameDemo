import XJSDKManager from "./XJSDKManager";

export default class SdkManager {

    /**提示框 */
    public static onShowToast(str: string) {
        XJSDKManager.ShowToast(str);
    }

    /**Sdk广告回调 */
    public static CreatBannerCallBack() {
        // let GameCtr = GameSceneCtr.instance;
        // if(GameCtr[GameCtr.UIState.YouLikeUI].visible) {
        //     return true;
        // }
        // else if(GameCtr[GameCtr.UIState.SkinTryOutUI].visible) {
        //     return true;
        // }
        // else if(GameCtr[GameCtr.UIState.FiveUpUI].visible) {
        //     return true;
        // }
        // else if(GameCtr[GameCtr.UIState.SettlementUI].visible) {
        //     return true;
        // }
        // else if(GameCtr[GameCtr.UIState.RoleGetUI].visible) {
        //     return true;
        // }
        // return false;
    }

    /**onShow回调 */
    public static onShowCallBack() {
        // if(!XJSDKManager.AdFlag) return;
        // GameSceneCtr.instance.openUI(GameSceneCtr.instance.UIState.MoreGameUI, "onShow");
    }

    /**监听导量按钮 */
    static onNavButtonListen(_btn, _data, _event) {
        // if(!Main.canShowMl) return;
        let _jumpFailCallback = () => {};

        if(_event != "MoreGameUI") {
            _jumpFailCallback = SdkManager.onJumpFailCallback;
        }

        (_btn as Laya.Button).offAll();
        (_btn as Laya.Button).on(Laya.Event.CLICK, this, XJSDKManager.NavigateMiniGame, [_data, _event, _jumpFailCallback]);
    }


    /**点击导量回调 */
    static onJumpFailCallback() {
        // GameSceneCtr.instance.openUI(GameSceneCtr.instance.UIState.MoreGameUI);
    }

    
    /**设置按钮图标信息 */      //width / 144
    static onConfigNavButton(_btn, _data, scale) {

        var _img = _btn.getChildByName(`IMG`);

        if (!_img) {
            _img = new Laya.Image();
            _btn.addChildAt(_img, 0);
            _img.width = _btn.width;
            _img.height = _btn.height;
            _img.pos(0, 0);
            _img.name = `IMG`;
        }
        _img.skin = _data.icon;
        if (_data.type == 2) {
            let _anim: Laya.Animation = _btn.getChildByName(`GifAnim`);
            if (!_anim) {
                _anim = new Laya.Animation();
                _anim.name = `GifAnim`;
                _btn.addChildAt(_anim, (_btn as Laya.Button)._children.length);
                _anim.pos(0, 0);
            }
            //_img.visible = false;

            _anim.visible = true;
            _anim.clear();
            _anim.loadImages(_data.iconLis);
            _anim.scale(scale, scale);
            _anim.interval = 120;
            _anim.play();
        } else {

            _img.visible = true;
            let _anim: Laya.Animation = _btn.getChildByName(`GifAnim`);
            if (_anim && _anim.visible) {
                _anim.stop();
                _anim.visible = false;
                _anim.clear();
                // console.log("动画已经隐藏");
            }
        }
        let _navName = _btn.getChildByName(`NavName`);
        if (_navName) {
            _btn.getChildByName(`NavName`).text = _data.name;
            _btn.getChildByName(`NavName`).text = "精品游戏";
        }
    }


}