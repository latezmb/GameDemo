export default class Tools {

    /**
     * 点击事件监听
     * @param btn
     * @param caller
     * @param listener
     * @param data
     */
    public static RegisterClickByCaller(btn: laya.ui.UIComponent,
        caller: any,
        listener: Function,
        data?: Object,
        bScale: boolean = true,
        soundPath: string = "",
        doStop: boolean = true
    ) {
        btn.on(Laya.Event.CLICK, this, () => {
            this.PlaySingleSound(soundPath, doStop);
            listener.apply(caller, data);
        });
        if (bScale) {
            // btn.anchorX = 0.5;
            // btn.anchorY = 0.5;
            btn.on(Laya.Event.MOUSE_DOWN, this, () => {
                btn.scale(0.9, 0.9, true);
            });
            btn.on(Laya.Event.MOUSE_UP, this, () => {
                btn.scale(1, 1, true);
            });
            btn.on(Laya.Event.MOUSE_OUT, this, () => {
                btn.scale(1, 1, true);
            });
        }
    }

    /**按下去播放的音效 */
    static PlaySingleSound(soundPath: string, doStop: boolean = true)  {
        // EntityManager.GetSoundManager.PlaySingleSound(soundPath, 1, doStop);
    }



    /**
     * 求两v2向量的距离 vc2_1 - vc2_2   返回距离（Number）
     * @param vc2_1 
     * @param vc2_2 
     */
    public static Vc2Distance(vc2_1: Laya.Vector2, vc2_2: Laya.Vector2): number {
        let _diff = new Laya.Vector2(vc2_1.x - vc2_2.x, vc2_1.y - vc2_2.y);
        let _distance = Math.sqrt(Math.pow(_diff.x, 2) + Math.pow(_diff.y, 2));
        return _distance;
    }


    /**
     * 求两v3向量x-z平面的距离 vc3_1 - vc3_2    返回距离（Number）
     * @param vc3_1 
     * @param vc3_2 
     */
    public static Vc3Distance(vc3_1: Laya.Vector3, vc3_2: Laya.Vector3): number {
        let _diff = new Laya.Vector3();
        Laya.Vector3.subtract(vc3_1, vc3_2, _diff);
        let _distance = Math.sqrt(Math.pow(_diff.x, 2) + Math.pow(_diff.z, 2));
        return _distance;
    }


    /**
     * 两向量相减 vc2_1 - vc2_2    返回相减后的向量
     * @param vc2_1 
     * @param vc2_2 
     */
    public static Vc2Subtract(vc2_1: Laya.Vector2, vc2_2: Laya.Vector2): Laya.Vector2 {
        let _diff = new Laya.Vector2(vc2_1.x - vc2_2.x, vc2_1.y - vc2_2.y);
        return _diff;
    }


    /**
     * 两向量相减 vc3_1 - vc3_2    返回相减后的向量
     * @param vc3_1 
     * @param vc3_2 
     */
    public static Vc3Subtract(vc3_1: Laya.Vector3, vc3_2: Laya.Vector3) : Laya.Vector3 {
        let _diff = new Laya.Vector3();
        Laya.Vector3.subtract(vc3_1, vc3_2, _diff);
        return _diff;
    }


    /**
     * 两向量相加 vc2_1 + vc2_2    返回相加后的向量
     * @param vc2_1 
     * @param vc2_2 
     */
    public static Vc2Add(vc2_1: Laya.Vector2, vc2_2: Laya.Vector2): Laya.Vector2 {
        let _add = new Laya.Vector2(vc2_1.x + vc2_2.x, vc2_1.y + vc2_2.y);
        return _add;
    }


    /**
     * 两向量相加 vc3_1 + vc3_2    返回相加后的向量
     * @param vc3_1 
     * @param vc3_2 
     */
    public static Vc3Add(vc3_1: Laya.Vector3, vc3_2: Laya.Vector3) : Laya.Vector3 {
        let _diff = new Laya.Vector3();
        Laya.Vector3.add(vc3_1, vc3_2, _diff);
        return _diff;
    }


    /**播放音效 */
    public static onPlaySound(path: string) {
        Laya.SoundManager.playSound(path, 1);
    }


    /**播放背景音乐 */
    public static onPlayMusic(path: string) {
        Laya.SoundManager.playMusic(path, 0);
    }


    /**停止播放背景音乐 */
    public static onStopMusic() {
        Laya.SoundManager.stopMusic();
    }
    

    /**
     * 卖量晃动，传入晃动控件即可
     * @param _sp 
     */
    public static onUINavRoate(_sp: Laya.Sprite) {
        let _time = 100;
        Laya.Tween.to(_sp, { rotation: -15 }, _time, Laya.Ease.linearIn, new Laya.Handler(this, () => {
            Laya.Tween.to(_sp, { rotation: 15 }, _time * 2, Laya.Ease.linearIn, new Laya.Handler(this, () => {
                Laya.Tween.to(_sp, { rotation: -15 }, _time * 2, Laya.Ease.linearIn, new Laya.Handler(this, () => {
                    Laya.Tween.to(_sp, { rotation: 0 }, _time, Laya.Ease.linearIn);
                }))
            }));
        }))
    }


    /**倒计时处理 列：传入60 返回 01:00， time 参数最大60 * 59 */ 
    public static countDownTimeHandler(time: number): string {
        if (time <= 0) return "00:00";
        let hm: any = Math.floor(time / 60);
        hm = (hm + "").length == 1 ? "0" + hm : hm;
        let second: any = Math.ceil(time % 60);
        second = (second + "").length == 1 ? "0" + second : second;
        return hm + ":" + second;
    }

    

}

export enum MusicPath {
    BackgroundMusic = "",         //背景音乐
}