export default class XJSDKManager extends Laya.Script {
    public static VideosAwardCallBack;//激励视频看完回调
    public static VideosUnfinshCallback;//激励视频未看完回调
    public static CreatBannerCallBack;//创建banner回调
    public static OnShowCallBack;//onShow回调
    public static SeverJsonPath = "https://domino-1300018364.cos.ap-chengdu.myqcloud.com/dominoSeverConfig.json";
    public static SeverJsonObj;//服务器json对象
    public static GameList = [];//开始界面导量数据
    public static GameListBack = [];//更多游戏界面导量数据
    public static GameListOver = [];//退出页面导量数据
    public static GameListBottom = [];//游戏底部导量数据
    public static GameListSide = [];//侧边导量数据     
    public static GameList_Back_two = [];//退出页面两个图标一行
    public static GameList_Back_rolling = [];//退出页面滚动条;
    public static SDKGuideVersion = 2;//SDK版本号

    public static AdFlag = false;              //误点
    public static GuideFlag_one = false;       //弹一个自动调整
    public static GuideFlag_two = false;       //视频宝箱
    public static SDKManagerLoadingFinsh = false;
    private static isIOSPhone = false;
    private static isAndroid = true;
    private static isIos = true;
    private static SceneValueOpen = false;
    private static UmaKey = "5eedab7adbc2ec08212afce1";
    private static XJSDKKey = "8767aeb593bf2774ab94f2fcff1853ae";
    private static BannerId = "adunit-c5f4ceafab4c7e85";
    private static InterId = "adunit-5dca7c3d395d17a6";
    private static VideoId = "adunit-cf070cfd2cce666a";

    private static BannerObj;
    private static VideoObj;
    private static InterObj;

    private static PlatformObj;
    private static InterReadyTime = 0;
    private static BannerAutorefrsh = 0;

    private static LoginFinsh = false;
    private static roleFinsh = false;
    constructor() {
        super();
    }
    public static onInit() {
        if (Laya.Browser.onMiniGame) {
            XJSDKManager.PlatformObj = Laya.Browser.window.wx;
            if (Laya.Browser.onIOS) {
                XJSDKManager.isIOSPhone = true;
            } else {
                XJSDKManager.isIOSPhone = false;
            }
            XJSDKManager.GetSeverData();
            XJSDKManager.onInitShareMenu();
            XJSDKManager.InitUma();
            XJSDKManager.InitSDK();
            XJSDKManager.GetSDKData();
            try {
                XJSDKManager.CheckUpdate();
            } catch (e) {

            }
            XJSDKManager.InitWXADCompoent();
            XJSDKManager.PlatformObj.onShow(() => {
                if (XJSDKManager.OnShowCallBack) {
                    XJSDKManager.OnShowCallBack();
                }

                XJSDKManager.CreatBanner();
            })
            Laya.timer.loop(1000, XJSDKManager, () => {
                XJSDKManager.InterReadyTime++;
                if (XJSDKManager.InterReadyTime >= 20) {
                    XJSDKManager.InterReadyTime = 20;

                }
                XJSDKManager.BannerAutorefrsh++;
                if (XJSDKManager.BannerAutorefrsh >= 30) {
                    XJSDKManager.CreatBanner();
                    console.log("自动刷新banner");
                }
            });
        }
    }
    //UI加载完成上报;
    public LoadingSDK() {
        XJSDKManager.PlatformObj.xiangjiao.load();
    }
    //首次点击上报
    public LoginSDK() {
        if (XJSDKManager.LoginFinsh)
            return;
        XJSDKManager.PlatformObj.xiangjiao.login();
        XJSDKManager.LoginFinsh = true;
    }
    //点击游戏开始按钮上报
    public startGameRole() {
        if (XJSDKManager.roleFinsh)
            return;
        XJSDKManager.PlatformObj.xiangjiao.role();
        XJSDKManager.roleFinsh = true;
    }
    public static ShowInterAD() {
        if (XJSDKManager.InterReadyTime != 20) {
            console.log("插屏未就绪");
            return;
        }
        if (XJSDKManager.InterObj) {
            XJSDKManager.InterReadyTime = 0;
            XJSDKManager.InterObj.show();
        }
    }
    public static ShowVideos() {
        if (XJSDKManager.VideoObj) {
            XJSDKManager.VideoObj.show();
            XJSDKManager.InterReadyTime = 0;
        }
    }
    private static InitUma() {
        XJSDKManager.PlatformObj.uma.init({
            appKey: XJSDKManager.UmaKey,
            useOpenid: false, // default true
            autoGetOpenid: false,
            debug: true
        });
    }
    private static onInitShareMenu() {
        XJSDKManager.PlatformObj.showShareMenu({
            withShareTicket: true,
            success: res => {
                console.log("分享面板初始化成功");
            }
        });
    }

    private static InitSDK() {
        if (Laya.Browser.onMiniGame) {
            var _obj = XJSDKManager.PlatformObj.getLaunchOptionsSync();
            var _sdkObj = XJSDKManager.PlatformObj.xiangjiao;
            _sdkObj.init(XJSDKManager.XJSDKKey, ``, _obj, (res) => {
                if (res.is_register_today == 1) {

                    XJSDKManager.PlatformObj.aldInit.default.Init();
                }
                var _openid = res.openid;
                XJSDKManager.PlatformObj.uma.setOpenid(_openid);

            });
        }
    }
    private static GetSeverData() {
        Laya.loader.load(XJSDKManager.SeverJsonPath, new Laya.Handler(XJSDKManager, () => {
            XJSDKManager.SeverJsonObj = Laya.loader.getRes(XJSDKManager.SeverJsonPath);
            console.log("服务器数据拉取完成");
            // if (XJSDKManager.SeverJsonObj.Android == "0") {
            //     XJSDKManager.isAndroid = true;
            // }
            // if (XJSDKManager.SeverJsonObj.Ios == "0") {
            //     XJSDKManager.isIos = true;
            // }
        }));
    }
    private static GetSDKData() {
        XJSDKManager.PlatformObj.xiangjiao.getGuide(XJSDKManager.SDKGuideVersion, (res) => {

            console.log("SDK数据源", res);
            if (res.info) {
                XJSDKManager.GameList = [];
                for (const iterator of res.info.gamelist) {
                    let element = iterator;
                    let _data = { name: element.name, icon: element.icon, appid: element.appid, path: element.path, extra: element.extra, type: element.icon_type, iconLis: element.icon_list, id: element.id };
                    XJSDKManager.GameList.push(_data);
                }
                XJSDKManager.GameListBack = [];
                for (const iterator of res.info.gamelist_back) {
                    let element = iterator;
                    let _data = { name: element.name, icon: element.icon, appid: element.appid, path: element.path, extra: element.extra, type: element.icon_type, iconLis: element.icon_list, id: element.id };
                    XJSDKManager.GameListBack.push(_data);
                }
                XJSDKManager.GameListOver = [];
                for (const iterator of res.info.gamelist_over) {
                    let element = iterator;
                    let _data = { name: element.name, icon: element.icon, appid: element.appid, path: element.path, extra: element.extra, type: element.icon_type, iconLis: element.icon_list, id: element.id };
                    XJSDKManager.GameListOver.push(_data);
                }
                XJSDKManager.GameListBottom = [];
                for (const iterator of res.info.gamelist_bottom) {
                    let element = iterator;
                    let _data = { name: element.name, icon: element.icon, appid: element.appid, path: element.path, extra: element.extra, type: element.icon_type, iconLis: element.icon_list, id: element.id };
                    XJSDKManager.GameListBottom.push(_data);
                }
                XJSDKManager.GameListSide = [];
                for (const iterator of res.info.gamelist_side) {
                    let element = iterator;
                    let _data = { name: element.name, icon: element.icon, appid: element.appid, path: element.path, extra: element.extra, type: element.icon_type, iconLis: element.icon_list, id: element.id };
                    XJSDKManager.GameListSide.push(_data);
                }
                XJSDKManager.GameList_Back_two = [];
                for (const iterator of res.info.gamelist_back_two) {
                    let element = iterator;
                    let _data = { name: element.name, icon: element.icon, appid: element.appid, path: element.path, extra: element.extra, type: element.icon_type, iconLis: element.icon_list, id: element.id };
                    XJSDKManager.GameList_Back_two.push(_data);
                }
                XJSDKManager.GameList_Back_rolling = [];
                for (const iterator of res.info.gamelist_back_rolling) {
                    let element = iterator;
                    let _data = { name: element.name, icon: element.icon, appid: element.appid, path: element.path, extra: element.extra, type: element.icon_type, iconLis: element.icon_list, id: element.id };
                    XJSDKManager.GameList_Back_rolling.push(_data);
                }
            }
            XJSDKManager.WxGetScenesValue(res.sence);
            if (res.flag == 1) {
                if (XJSDKManager.SceneValueOpen) {
                    if (XJSDKManager.isIOSPhone && XJSDKManager.isIos) {
                        XJSDKManager.AdFlag = true;
                    }
                    if (!XJSDKManager.isIOSPhone && XJSDKManager.isAndroid) {
                        XJSDKManager.AdFlag = true;
                    }
                }
            }
            if (res.guide_flag_one == 1) {
                if (XJSDKManager.SceneValueOpen) {

                    if (XJSDKManager.isIOSPhone && XJSDKManager.isIos) {
                        XJSDKManager.GuideFlag_one = true;
                    }
                    if (!XJSDKManager.isIOSPhone && XJSDKManager.isAndroid) {
                        XJSDKManager.GuideFlag_one = true;
                    }
                }
            }
            if (res.guide_flag_two == 1) {
                if (XJSDKManager.SceneValueOpen) {
                    if (XJSDKManager.isIOSPhone && XJSDKManager.isIos) {
                        XJSDKManager.GuideFlag_two = true;
                    }
                    if (!XJSDKManager.isIOSPhone && XJSDKManager.isAndroid) {
                        XJSDKManager.GuideFlag_two = true;
                    }
                }
            }
            if (XJSDKManager.GameList.length > 0 && XJSDKManager.GameListBack.length > 0 && XJSDKManager.GameListOver.length > 0) {
                XJSDKManager.SDKManagerLoadingFinsh = true;
                console.log(`当前脚本状态+adflag+${XJSDKManager.AdFlag} + one +${XJSDKManager.GuideFlag_one} + two + ${XJSDKManager.GuideFlag_two} + sceneOpen + ${XJSDKManager.SceneValueOpen} + IOSPhone + ${XJSDKManager.isIOSPhone} + isAndroid + ${XJSDKManager.isAndroid} + isIos + ${XJSDKManager.isIos}`);
            } else {
                Laya.timer.once(2000, XJSDKManager, XJSDKManager.GetSDKData);
            }
        });
    }
    private static WxGetScenesValue(_xjScenes) {
        if (Laya.Browser.onMiniGame) {
            var _obj = XJSDKManager.PlatformObj.getLaunchOptionsSync();
            var _userScenesValue = _obj.scene;

            for (const iterator of _xjScenes) {
                if (iterator == _obj.scene) {
                    XJSDKManager.SceneValueOpen = true;
                    break;
                }
            }
            console.log("当前场景值", _obj.scene);
        }
    }
    /**type： 1=gamelist 2=gameback 3= gameover 4=gamelist_Bottom 5= gamelist_side 6 = gamelist_back_two 7 = gamelist_back_rolling*/
    static ExposureData(_type) {
        var _data = [];
        switch (_type) {
            case 1:
                _data = XJSDKManager.GameList;
                break;
            case 2:
                _data = XJSDKManager.GameListBack;
                break;
            case 3:
                _data = XJSDKManager.GameListOver;
                break;
            case 4:
                _data = XJSDKManager.GameListBottom;
                break;
            case 5:
                _data = XJSDKManager.GameListSide;
                break;
            case 6:
                _data = XJSDKManager.GameList_Back_two;
                break;
            case 7:
                _data = XJSDKManager.GameList_Back_rolling;
                break;
        }
        if (_data && _data.length > 0) {
            XJSDKManager.ExposureDataEvent(_data);
            console.log("曝光数据下标:" + _type);
        }
    }
    static ExposureDataEvent(_exprosureData) {
        if (!Laya.Browser.onMiniGame) {
            return;
        }
        var _dataArr: Array<any> = [];

        if (_exprosureData != null && _exprosureData != undefined) {
            if (_exprosureData.length > 0) {
                for (let index = 0; index < _exprosureData.length; index++) {
                    const element = _exprosureData[index];
                    _dataArr.push(element.id);
                }
                XJSDKManager.PlatformObj.xiangjiao.exposureMiniProgram(_dataArr);
                console.log(`曝光数据发送完成`, _dataArr);
            }
        }
    }
    static clickMiniProgram(id) {
        if (!Laya.Browser.onMiniGame) {
            return;
        }
        XJSDKManager.PlatformObj.xiangjiao.clickMiniProgram(id);
    }
    static SuccessNavMiniGame(id) {
        if (!Laya.Browser.onMiniGame) {
            return;
        }
        XJSDKManager.PlatformObj.xiangjiao.navigateToMiniProgram(id);
    }
    //初始化微信广告
    static InitWXADCompoent() {
        if (Laya.Browser.onMiniGame) {
            XJSDKManager.CreatBanner();
            XJSDKManager.CreatInter();
            XJSDKManager.CreatVideo();
        }
    }
    static CreatBanner() {

        if (!Laya.Browser.onMiniGame)
            return;
        if (XJSDKManager.BannerObj) {
            XJSDKManager.BannerObj.destroy();
        }
        if (XJSDKManager.BannerId) {


            var bHeight = Laya.Browser.clientHeight;
            var bwidth = Laya.Browser.clientWidth;

            XJSDKManager.BannerObj = XJSDKManager.PlatformObj.createBannerAd({
                adUnitId: XJSDKManager.BannerId,
                style: {
                    left: 0,
                    top: 0,
                    width: 200
                }
            });
            XJSDKManager.BannerObj.onError((res) => {
                console.log("广告banner错误信息", res);
            });
            XJSDKManager.BannerObj.onResize((res) => {
                XJSDKManager.PlatformObj.getSystemInfo({
                    success(res2) {
                        if (res2.model.indexOf("XS Max") != -1 || res2.model.indexOf("iPhone X") != -1) {
                            var posy = bHeight - res.height - 10;
                            var posx = bwidth / 2 - (res.width / 2);
                            XJSDKManager.BannerObj.style.top = posy;
                            XJSDKManager.BannerObj.style.left = posx;
                        }
                        else {
                            var posy = bHeight - res.height;
                            var posx = bwidth / 2 - (res.width / 2);
                            XJSDKManager.BannerObj.style.top = posy;
                            XJSDKManager.BannerObj.style.left = posx;
                        }
                    }
                })
            });
            if (XJSDKManager.CreatBannerCallBack)
                var _bannerShow = XJSDKManager.CreatBannerCallBack();
            if (_bannerShow) {
                XJSDKManager.BannerObj.show();
            } else {
                XJSDKManager.BannerObj.hide();
            }
        }
        XJSDKManager.BannerAutorefrsh = 0;
    }
    static ShowBanner() {
        if (XJSDKManager.BannerObj) {
            XJSDKManager.BannerObj.show();
        }
    }
    static HideBanner() {
        if (XJSDKManager.BannerObj) {
            XJSDKManager.BannerObj.hide();
        }
    }
    static CheckUpdate() {
        if (Laya.Browser.onMiniGame) {
            var _updateManager = XJSDKManager.PlatformObj.getUpdateManager();

            _updateManager.onCheckForUpdate(function (res) {
                // 请求完新版本信息的回调
                console.log(res.hasUpdate)
            })

            _updateManager.onUpdateReady(function () {
                XJSDKManager.PlatformObj.showModal({
                    title: '更新提示',
                    content: '新版本已经准备好，是否重启应用？',
                    success(res) {
                        if (res.confirm) {
                            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                            _updateManager.applyUpdate()
                        }
                    }
                })
            })
        }
    }
    static CreatVideo() {
        if (XJSDKManager.VideoId) {
            XJSDKManager.VideoObj = XJSDKManager.PlatformObj.createRewardedVideoAd({
                adUnitId: XJSDKManager.VideoId
            });
            XJSDKManager.VideoObj.onError(err => {
                console.log("激励视频广告出错" + JSON.stringify(err));
            });
            XJSDKManager.VideoObj.onClose(res => {
                if (res && res.isEnded || res === undefined) {
                    if (XJSDKManager.VideosAwardCallBack)
                        XJSDKManager.VideosAwardCallBack();
                    XJSDKManager.UpUmaData(4);
                }
                else {
                    if (XJSDKManager.VideosUnfinshCallback)
                        XJSDKManager.VideosUnfinshCallback();
                    XJSDKManager.UpUmaData(5);
                }
            });
        }
    }
    static CreatInter() {
        try {
            XJSDKManager.InterObj = XJSDKManager.PlatformObj.createInterstitialAd({
                adUnitId: XJSDKManager.InterId
            })
        } catch (e) {
            console.error(`插屏初始化失败`);
        }
    }
    public static Vibrate() {
        if (Laya.Browser.onMiniGame) {
            XJSDKManager.PlatformObj.vibrateShort({
                success: res => {
                    console.log('短震动成功');
                },
                fail: (err) => {
                    console.log('短震动失败');
                }
            });
        }
    }
    public static ShowToast(value) {
        if (Laya.Browser.onMiniGame) {
            XJSDKManager.PlatformObj.showToast({
                title: value,
                duration: 2000,
                icon: "none"
            })
        } else {
            console.log(value);
        }
    }

    public static NavigateMiniGame(_data, _event, jumpFailCallback = null, _updata = true) {
        let _AldData = { [_event]: _data.name };
        console.log(`点击小游戏跳转`, _AldData);
        if (Laya.Browser.onMiniGame) {
            if (_updata)
                XJSDKManager.clickMiniProgram(_data.id);
            let _appId = _data.appid;
            let _path = _data.path;
            XJSDKManager.PlatformObj.navigateToMiniProgram({
                appId: _appId,
                path: _path,
                success(res) {
                    var _value = `成功跳转${_data.name}游戏`
                    var params = {
                        eventName: '点击游戏',
                        subEventName: _value
                    }
                    XJSDKManager.SuccessNavMiniGame(_data.id);
                    XJSDKManager.setUmaData(`nimiGamePass`, { "sucess": _data.name });
                },
                fail() {
                    if (jumpFailCallback) {
                        jumpFailCallback();
                    }
                    XJSDKManager.setUmaData(`nimiGamePass`, { "fail": _data.name });
                }
            })
        }
    }
    static setUmaData(_eventId, _value) {
        try {
            XJSDKManager.PlatformObj.uma.trackEvent(_eventId, _value);
        } catch (e) {

        }
    }

    //_type类型, 0:自动播放视频 1:点击开始游戏按钮 2:进入游戏  3:游戏结束
    public static UpUmaData(_type) {
        var _eventId = "";
        switch (_type) {
            case 0:
                _eventId = "CloseInter"
                break;
            case 1:
                _eventId = "ClickStartBtn"
                break;
            case 2:
                _eventId = "gameStart"
                break;
            case 3:
                _eventId = "gameover"
                break;
            case 4:
                _eventId = "VideoSuccess"
                break;
            case 5:
                _eventId = "VideoFail"
                break;

        }
        XJSDKManager.setUmaData(_eventId, "");
    }
}