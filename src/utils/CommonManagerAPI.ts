export default class CommonManagerAPI extends Laya.Script {
    /** @prop {name:intType, tips:"整数类型示例", type:Int, default:1000}*/
    public intType: number = 1000;
    /** @prop {name:numType, tips:"数字类型示例", type:Number, default:1000}*/
    public numType: number = 1000;
    /** @prop {name:strType, tips:"字符串类型示例", type:String, default:"hello laya"}*/
    public strType: string = "hello laya";
    /** @prop {name:boolType, tips:"布尔类型示例", type:Bool, default:true}*/
    public boolType: boolean = true;
    // 更多参数说明请访问: https://ldc2.layabox.com/doc/?nav=zh-as-2-4-0

    constructor() { super(); }

    onEnable(): void {
    }

    onDisable(): void {
    }

    static onUINavRoate(_sp: Laya.Sprite) {
        let _time = 100;
        Laya.Tween.to(_sp, { rotation: -15 }, _time, Laya.Ease.linearIn, new Laya.Handler(this, () => {
            Laya.Tween.to(_sp, { rotation: 15 }, _time * 2, Laya.Ease.linearIn, new Laya.Handler(this, () => {
                Laya.Tween.to(_sp, { rotation: -15 }, _time * 2, Laya.Ease.linearIn, new Laya.Handler(this, () => {
                    Laya.Tween.to(_sp, { rotation: 0 }, _time, Laya.Ease.linearIn);
                }))
            }));
        }))
    }
    public static GetPointDistance(_point1: Laya.Vector2, _point2: Laya.Vector2): number {
        var _dx = Math.abs(_point1.x - _point2.x);
        var _dy = Math.abs(_point1.y - _point2.y);
        var _dis = Math.sqrt(Math.pow(_dx, 2) + Math.pow(_dy, 2));
        return _dis;
    }
    static GetAngle(start: Laya.Vector2, end: Laya.Vector2) {
        // var _diffX = end.x - start.x;
        // var _diffY = end.y - start.y;

        // return 360 * Math.atan(_diffY / _diffX) / (2 * Math.PI);
        return 360 * (Math.atan2(start.x, start.y) - Math.atan2(end.x, end.y) / (2 * Math.PI));
    }
    static AddVector(_v1: Laya.Vector3, _v2: Laya.Vector3) {
        let _v = new Laya.Vector3(0, 0, 0);
        Laya.Vector3.add(_v1, _v2, _v);
        return _v;
    }
    static SubVector(_v1: Laya.Vector3, _v2: Laya.Vector3) {
        let _v = new Laya.Vector3(0, 0, 0);
        Laya.Vector3.subtract(_v1, _v2, _v);
        return _v;
    }
    static ScaleVector(_v1: Laya.Vector3, b: number) {
        let _v = new Laya.Vector3(0, 0, 0);
        Laya.Vector3.scale(_v1, b, _v);
        return _v;
    }
    static SavePointNum(x) {

        var f = parseFloat(x);
        if (isNaN(f)) {
            return;
        }
        f = Math.round(x * 100) / 100;
        return f;
    }
    public static RandomSortArray(_aimArray: Array<any>): Array<any> {
        let _initArray = _aimArray;
        let _length = _aimArray.length;
        var _outArray: Array<any> = [];
        for (var _num = 0; _num < _length; _num++) {
            var _index = Math.floor(Math.random() * _initArray.length);

            _outArray.push(_initArray[_index]);
            _initArray.splice(_index, 1);
        }
        return _outArray;

    }
    static UIAnim(_sp) {
        if (_sp != null && _sp != undefined)
            _sp.scale(0, 0);
        var _pos = { scale: 0 };
        Laya.Tween.to(_pos, {
            scale: 1, update: new Laya.Handler(this, function () {
                if (_sp != null && _sp != undefined) {
                    _sp.scale(_pos.scale, _pos.scale);
                }
            })
        }, 100, Laya.Ease.linearIn, new Laya.Handler(this, function () {
        }));
    }
    static ClearAllAnim() {
        Laya.Tween.clearAll(this);
    }
    //计算包围盒覆盖面积点
    static GetBoxPoint(p1, p2, p1length, p2length):Laya.Point[]{
        var _arrPoint = [];
        for (let _p11 = 0; _p11 < p1length * 2; _p11++) {
            for (let _p22 = 0; _p22 < p2length * 2; _p22++) {
                var _point = new Laya.Point(p1 - p1length + _p11, p2 - p2length + _p22);
                _arrPoint.push(_point);
            }
        }
        return _arrPoint;
    }
    static PlaySound(path:string){
        Laya.SoundManager.playSound(path);
    }
}